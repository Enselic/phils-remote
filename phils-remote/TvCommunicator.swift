//
//  TvCommunicator.swift
//  Better Philips TV Remote
//
//  Created by Martin Nordholts on 2016-11-16.
//  Copyright © 2016 Martin Nordholts. All rights reserved.
//

import Foundation

typealias NameCallback = (_ name: String, _ vendor: String, _ model: String) -> Void

protocol TvListener {
    func tvFound()
    func tvLost()
}

/**
 * Must be used from main thread.
 */
class TvCommunicator {
    
    // To not block main thread when doing network stuff etc
    let workerThread: DispatchQueue
    
    let worker: TvCommunicatorWorker
    
    init(nameCallback: @escaping NameCallback) {
        self.workerThread = DispatchQueue(label: "TvCommunicator.workerThread")
        self.worker = TvCommunicatorWorker(nameCallback: nameCallback, workerThread: workerThread)
    }
    
    /**
     * Initializes data structures on the worker thread so it can be used by the client.
     */
    func discover() {
        workerThread.async {
            self.worker.discover()
        }
    }
    
    func setHdmi(_ n: Int) {
        workerThread.async {
            self.worker.tvHttpPost(json: "{ \"id\": \"hdmi\(n)\" }", url: "/1/sources/current")
        }
    }
    
    func sendKey(key: String) {
        workerThread.async {
            self.worker.tvHttpPost(json: "{ \"key\": \"\(key)\" }", url: "/1/input/key")
        }
    }
}

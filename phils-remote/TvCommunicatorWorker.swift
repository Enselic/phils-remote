//
//  TvCommunicatorWorker.swift
//  Better Philips TV Remote
//
//  Created by Martin Nordholts on 2016-11-19.
//  Copyright © 2016 Martin Nordholts. All rights reserved.
//

import Foundation



func dataCallBackMainThread(_ socket: CFSocket?, type: CFSocketCallBackType, address: CFData?, dataPtr: UnsafeRawPointer?, infoPtr: UnsafeMutableRawPointer?) {
    NSLog("dataCallBack")
    
    if type == CFSocketCallBackType.dataCallBack, let dataPtr = dataPtr, let infoPtr = infoPtr, let address = address {
        
        let meself = Unmanaged<TvCommunicatorWorker>.fromOpaque(infoPtr).takeUnretainedValue()
        let data = Unmanaged<CFData>.fromOpaque(dataPtr).takeUnretainedValue()
        meself.workerThread.async {
            meself.dataCallBack(address, data)
        }
    }
}


let voodooPort : UInt16 = 2323
let jointspacePort : UInt16 = 1925



extension in_addr_t {
    func toIp() -> String {
        return "\(self >> 0 & 0xff).\(self >> 8 & 0xff).\(self >> 16 & 0xff).\(self >> 24 & 0xff)"
    }
}



/**
 * May only be called from TvCommunicator worker thread.
 */
class TvCommunicatorWorker {
    
    let NAME = "Simple Philips TV Remote"
    
    let nameCallback: NameCallback
    let workerThread: DispatchQueue
    var tvIp: String?
    var voodooSocket: CFSocket?

    init(nameCallback: @escaping NameCallback, workerThread: DispatchQueue) {
        self.nameCallback = nameCallback
        self.workerThread = workerThread
    }
    
    func discover() {
        var context = CFSocketContext()
        context.info = UnsafeMutableRawPointer(Unmanaged.passUnretained(self).toOpaque())
        self.voodooSocket = CFSocketCreate(kCFAllocatorDefault, PF_INET, SOCK_DGRAM, IPPROTO_UDP,
                                                        CFSocketCallBackType.dataCallBack.rawValue, dataCallBackMainThread, &context)
        var receiveAddress = self.createSockaddrData(addr: INADDR_ANY)
        let cdata = NSData(bytes: &receiveAddress, length: MemoryLayout<sockaddr_in>.size) as CFData
        CFSocketSetAddress(self.voodooSocket, cdata)
        
        let source = CFSocketCreateRunLoopSource(nil, self.voodooSocket, 42)
        CFRunLoopAddSource(CFRunLoopGetMain(), source, CFRunLoopMode.commonModes)
        
        
        
        
        
        var sa = self.createSockaddrData(addr: INADDR_BROADCAST)
        let address = NSData(bytes: &sa, length: MemoryLayout.size(ofValue: sa)) as CFData
        var enable: Int32 = 1
        let sockoptres = withUnsafeBytes(of: &enable) { (enablePtr: UnsafeRawBufferPointer) -> Int32 in
            return setsockopt(CFSocketGetNative(self.voodooSocket), SOL_SOCKET, SO_BROADCAST, enablePtr.baseAddress, 4/*TODO: don't hardcode*/);
        }
        
        NSLog("sockoptres = \(sockoptres)")
        
        
        let error = CFSocketSendData(self.voodooSocket, address, self.createMessageData() as CFData, 10)
        NSLog("CFSocketSendData error=\(error.rawValue)")        
    }

    func tvHttpPost(json: String, url: String) {
        let url = URL(string: "http://\(tvIp!):1925\(url)")!
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = "POST"
        urlRequest.httpBody = json.data(using: .utf8)
        URLSession.shared.downloadTask(with: urlRequest).resume()
    }

    func dataCallBack(_ address: CFData, _ data: CFData) {
        if CFDataGetLength(data) != 344 {
            NSLog("datagram size is \(CFDataGetLength(data)) and not not 344; can't be voodoo message")
            return
        }
        if CFDataGetLength(address) != MemoryLayout<sockaddr_in>.size {
            NSLog("address not a sockaddr_in; ignoring voodoo message too")
            return
            
        }
        
        let dataBytes = CFDataGetBytePtr(data)!
        
        
        func toString(p: UnsafePointer<UInt8>?) -> String {
            let maxLenInclNullChar = 96
            var lastByte = p!.advanced(by: maxLenInclNullChar - 1)
            var nullFound = false
            while p!.distance(to: lastByte) > 0 {
                if lastByte.pointee != 0 {
                    nullFound = true
                    break
                }
                lastByte = lastByte.predecessor()
            }
            return nullFound ? String.decodeCString(p!, as: UTF8.self)!.result : "<unknown>"
        }
        let name = toString(p: dataBytes.advanced(by: 56))
        let vendor = toString(p: dataBytes.advanced(by: 56 + 96))
        let model = toString(p: dataBytes.advanced(by: 56 + 96 + 96))
        
        if NAME != name {
            DispatchQueue.main.async {
                self.nameCallback(name, vendor, model)
            }
            
            let p = CFDataGetBytePtr(address)
            let pointee = p?.withMemoryRebound(to: sockaddr_in.self, capacity: 1) {
                return $0.pointee
            }
            tvIp = pointee?.sin_addr.s_addr.toIp()
            NSLog("got ip \(tvIp)")
        } else {
            NSLog("Self-reply to broadcast; ignoring")
        }
        
    }
    
    private func createMessageData() -> Data {
        let version32B: [UInt8] = [
            0x03, // versionFlags = little_endian | 32bit_serials
            0x01, // major_version
            0x08, // minor_version
            0x00, // micro_version
            0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00,  0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00,  0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00,  0x00, 0x00, 0x00, 0x00,
            ]
        let messageTypeUInt32LE: [UInt8] = [
            0x01, 0x00, 0x00, 0x00 // broadcast
        ]
        let playerInfoUInt32LE: [UInt8] = [
            0x03, 0x00, 0x00, 0x00 // level2 | packet
        ]
        let uuid16B: [UInt8] = [
            0xcb, 0x14, 0xd1, 0x2c, 0x39, 0x27, 0x40, 0xc5,
            0xa8, 0xe2, 0x5e, 0x0c, 0x64, 0xa5, 0x5e, 0xe2, // Random number
        ]
        
        var data = Data()
        func data_append(array: [UInt8]) {
            data.append(array, count: array.count)
        }
        func data_append(string: String) {
            data.append(string.data(using: String.Encoding.utf8)!)
            
            // Padding
            let wantedLength = 96
            let padding = wantedLength - string.lengthOfBytes(using: String.Encoding.ascii)
            let z: [UInt8] = [ 0x00 ]
            for _ in 1...padding {
                data_append(array: z)
            }
        }
        data_append(array: version32B)
        data_append(array: messageTypeUInt32LE)
        data_append(array: playerInfoUInt32LE)
        data_append(array: uuid16B)
        data_append(string: NAME)
        data_append(string: "https://setofskills.com")
        data_append(string: "v0.1")
        
        return data
    }
    
    private func createSockaddrData(addr: UInt32) -> sockaddr_in {
        let voodooPort: UInt16 = 2323 // DirectFB voodoo
        return sockaddr_in(
            sin_len: UInt8(MemoryLayout<sockaddr_in>.size),
            sin_family: sa_family_t(AF_INET),
            sin_port: CFSwapInt16HostToBig(voodooPort),
            sin_addr: in_addr(s_addr: CFSwapInt32HostToBig(addr)),
            sin_zero: (0, 0, 0, 0, 0, 0, 0, 0))
    }
    
}

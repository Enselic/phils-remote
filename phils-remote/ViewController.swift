//
//  ViewController.swift
//  Better Philips TV Remote
//
//  Created by Martin Nordholts on 2016-11-02.
//  Copyright © 2016 Martin Nordholts. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    // MARK: Properties
    @IBOutlet weak var statusLabel: UILabel!
    
    var tvCommunicator: TvCommunicator?
    
    var noTvInfo: UIViewController?

    override func viewDidLoad() {
        noTvInfo = storyboard?.instantiateViewController(withIdentifier: "NoTvInfo")
        self.tvCommunicator = TvCommunicator() { (_ name: String, _ vendor: String, _ model: String) -> Void in
            //self.statusLabel.text = self.combineVendorNameModel(vendor, name, model)
        }
        tvCommunicator?.discover()
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 5) {
            self.present(self.noTvInfo!, animated: true)
        }
    }
    
    func combineVendorNameModel(_ vendor: String, _ name: String, _ model: String) -> String {
        // TODO: Capture more cases and add unit test
        if (name.contains(vendor)) {
            return "\(name) \(model)"
        } else {
            return "\(vendor) \(name) \(model)"
        }
    }

    @IBAction func home() {
        tvCommunicator?.sendKey(key: "Home")
    }
    @IBAction func off() {
        tvCommunicator?.sendKey(key: "Standby")
    }
    @IBAction func left() {
        tvCommunicator?.sendKey(key: "CursorLeft")
    }
    @IBAction func right() {
        tvCommunicator?.sendKey(key: "CursorRight")
    }
    @IBAction func up() {
        tvCommunicator?.sendKey(key: "CursorUp")
    }
    @IBAction func down() {
        tvCommunicator?.sendKey(key: "CursorDown")
    }
    @IBAction func ok() {
        tvCommunicator?.sendKey(key: "Confirm")
    }
    @IBAction func back() {
        tvCommunicator?.sendKey(key: "Back")
    }
    @IBAction func volDown() {
        tvCommunicator?.sendKey(key: "VolumeDown")
    }
    @IBAction func mute() {
        tvCommunicator?.sendKey(key: "Mute")
    }
    @IBAction func volUp() {
        tvCommunicator?.sendKey(key: "VolumeUp")
    }
    @IBAction func hdmi1() {
        tvCommunicator?.setHdmi(1)
    }
    @IBAction func hdmi2() {
        tvCommunicator?.setHdmi(2)
    }
}

